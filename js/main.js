/**
 * Author:  Milton Plotkin
 * Date:    18/01/2022 
 */
(function(){

    class Main {

        constructor() {
            this.initialize();
        }

        initialize() {
            this.onInputChange = _.debounce(this.onInputChange.bind(this), 100);
            $(".js-input").on("change paste keyup keydown", this.onInputChange);


            this.process($(".js-input").val());
        }
    
        // Whenever the input is changed, re-process the text.
        onInputChange(event) {
            $(".js-output").html(""); // Clear the output first.
            this.process($(event.target).val());
        }

        process(str) {

            // First, split along each line.
            let lines = str.split("\n");
            let l = 0;

            let blocks = [
                new NumBlock(),
                new NameBlock(),
                new YieldBlock(),
                new IngredientsBlock(),
                new StepsBlock(),
                new StepsBlock(),
                new StepsBlock(),
                new StepsBlock(),
                new StepsBlock(),
                new StepsBlock(),
                new StepsBlock(),
                new StepsBlock(),
                new StepsBlock(),
                new StepsBlock(),
                new StepsBlock(),
                new StepsBlock(),
                new StepsBlock(),
                new StepsBlock(),
                new StepsBlock(),
                new StepsBlock(),
            ];

            // Step through each line an interpret the values.
            while (lines.length && blocks.length) {

                if (blocks[0].processLine(lines[0])) {
                    blocks[0].render();
                    blocks.shift();
                }
                else {
                    lines.shift();
                }
            }
            if (blocks.length) blocks[0].render();
        }
    }

    /**
     * Each copy+paste "block" gets its own class.
     */
    class Block {

        constructor() {
            this.str = ""; // String which the user will copy + paste. Gets added to on processing.
        }

        render() {

            let $el = $("<div>", {
                class: "block"
            });
            $el.html(this.str);

            $(".js-output").append($el);

            $el.on("click", () => {
                navigator.clipboard.writeText(this.str);
                $(".block").removeClass("is-active");
                $el.addClass("is-active");
            });
        }

        addStr(str) {
            this.str = this.str + str;
        }

        processLine(str) {
            // Check the line string, and concat to this.str is applicable.
            // Return true if this line indicates the start of another block.
            
            return false;
        }
    }

    /** Recipe number block */
    class NumBlock extends Block {

        /**
         * Example:
         * 
         * Recipe 62
         */
        processLine(str) {

            try {
                let num = str.match(/Recipe\s+(\d+)/i)[1];
                this.addStr(num.trim().padStart(3, "0"));
                return true;
            }
            catch (err) {
                return false;
            }
        }
    }

    /** Recipe name block */
    class NameBlock extends Block {

        /**
         * Example:
         * 
         * IDAHO POTATOES 	YIELD: 5 portions
         * 
         * Comes directly after the recipes line. Grab it immediately.
         */
        processLine(str) {

            try {
                let name = str.match(/([\w ()-/]+)/)[1];
                if (name.match(/recipe/i)) return false;

                name = name.trim().toLowerCase();
                name = name.replace(/^./, name[0].toUpperCase()); // Make the first character in the string uppercase.

                this.addStr(name.trim());
                return true;
            }
            catch (err) {
                return false;
            }
        }
    }

    /** Yield block */
    class YieldBlock extends Block {

        /**
         * Example:
         * 
         * IDAHO POTATOES 	YIELD: 5 portions
         * 
         * Comes directly after the recipes line. Grab it immediately.
         * NOT ALL RECIPES WILL HAVE A YIELD. In such instances, don't create the block.
         */
        processLine(str) {

            try {
                let val = str.match(/(yield[:]?|tool[:]?)\s(.*)/i)[2];
                this.addStr(val.trim());
                return true;
            }
            catch (err) {
                return true;
            }
        }
    }

    /** Ingredients block */
    class IngredientsBlock extends Block {

        /**
         * Example:
         * 
         *  Ingredients	Quantity
            Potatoes, medium to large (uniform in size)	5
            Foil	
            Rock salt	500 g
            Sour cream	150 ml
            Chives, finely chopped	¼ bunch
            Paprika	5 g
         */
        processLine(str) {

            try {
                let isBlank = false;
                if (!this.str) isBlank = true;

                // Each row has two columns; the ingredient and the quantity.
                let vals = str.split("\t");
                let col1 = (vals.length > 0) ? vals[0].trim() : "";
                let col2 = (vals.length > 1) ? vals[1].trim() : "";

                // Skip the row which says "Ingredients   Quantity".
                if (col1.match(/^ingredient/i)) return false;

                // When the "Step   Method" row is reached, stop.
                if (col1.match(/^step/i) && col2.match(/^method/i)) {
                    this.addStr(`       ]
    }`);
                    return true; 
                }
                
                // Skip blank rows.
                if (!col1 && !col2) return false;

                // If only the first column is defined, assume that there's a title.
                let title = ""; //(col1 && !col2) ? col1 : "";

                // Close the previous object and start a new one.
                if (title && !isBlank) {
                    this.addStr(`       ]
    },
    {
        title: "${title}",
        items: [
`);
                }
                // Start the array.
                else if (isBlank) {
                    this.addStr(`
    {
        title: "${title}",
        items: [
`);
                }
                else {
                    // Add the element.
                    this.addStr(`           {name: "${col1}", qty: "${col2}"},
`);
                }
            }
            catch (err) {
                return false;
            }
        }
    }

    /** Steps block */
    class StepsBlock extends Block {

        /**
         * Example:
         * 
         * Step	Method
            1	Preheat the oven to 180 °C.
            2	Wash potatoes and dry well.
         */
        processLine(str) {

            let isBlank = !Boolean(this.str);

            try {

                // Each row has two columns; the number/bullet point, and the text.
                let vals = str.split("\t");
                let num = (vals.length > 0) ? vals[0].trim() : "";
                let text = (vals.length > 1) ? vals[1].trim() : "";

                // Skip the "Step   Method" row.
                if (num.match(/^step/i) && text.match(/^method/i)) return false;

                // If the num is a word (and not a number or a bullet point), then start a new block.
                // Only do this if this block has some other content in it.
                if (!isBlank && num.match(/[\d\W]/i) && !text) return true;

                // Otherwise, add the text (or num, which will be a title if there's no text).
                this.addStr((text || num) + "\n");

                return false;
            }
            catch (err) {
                return false;
            }
        }
    }

    let main = new Main();
})();